import path from 'node:path';
import { defineConfig, loadEnv } from 'vite';
import vue from '@vitejs/plugin-vue';
import checker from 'vite-plugin-checker';
import ElementPlus from 'unplugin-element-plus/vite';

const app = __dirname;
// const targetPath = import.meta.env.VITE_HTTP_DATA_SERVICE_URL;
// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  const env = loadEnv(mode, app);
  return {
    plugins: [
      vue(),
      checker({
        eslint: {
          lintCommand: 'eslint ./src/**/*.{js,jsx,ts,tsx,vue}',
        },
      }),
      ElementPlus({
        useSource: true,
      }),
    ],
    resolve: {
      alias: {
        '@': path.resolve(__dirname, './src'),
      },
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@use "@/styles/element-ui/index.scss" as *;',
        },
      },
    },
    // 配置代理
    server: {
      proxy: {
        '/api': {
          target: env.VITE_HTTP_AUTH_SERVICE_PROXY_URL,
          changeOrigin: true,
          rewrite: path => path.replace(/^\/api/, ''),
        },
      },
    },
  };
});
