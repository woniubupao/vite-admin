const theme = require('./src/styles/variables.cjs');
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './index.html',
    './src/**/*.{js,ts,jsx,tsx,vue}',
  ],
  theme: {
    extend: {
      colors: {
        b: theme.basicColors,
        t: theme.textColors,
        gray: theme.grayColors,
      },
    },
  },
  plugins: [],

  corePlugins: {
    // https://tailwindcss.com/docs/preflight
    preflight: false, // 关闭默认样式
  },
};
