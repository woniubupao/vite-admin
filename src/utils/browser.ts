import { ElMessage } from 'element-plus';

// 复制
export function copy(value: string) {
  if (!navigator.clipboard) {
    ElMessage.warning('复制失败，请手动复制！');
    return;
  }
  navigator.clipboard.writeText(value).then(() => {
    ElMessage.success('复制成功！');
  });
}
