import type { RouteRecordRaw } from 'vue-router';
import type { MenuItem } from '@/api/login';
import type { AuthMenu } from '@/components/uni-components/uni-layout/uni-menu/type';

// 格式化菜单
export function formatMenu(menu: MenuItem[], routes: RouteRecordRaw[]): AuthMenu[] {
  const authMenu: AuthMenu[] = [];
  menu.forEach((item) => {
    const route = routes.find(route => route.name === item.front_name);
    const newMenu: AuthMenu = {
      id: `${item.id}`,
      title: item.title,
      icon: (route?.meta?.icon as string) || '',
      name: item.front_name || '',
    };
    if (item.child && item.child.length) {
      newMenu.children = formatMenu(item.child, routes);
    }
    authMenu.push(newMenu);
  });
  return authMenu;
}
