export function isNullable(val: unknown): val is undefined | null {
  return val === undefined || val === null;
}

export function isType<T>(val: unknown, type: string): val is T {
  const typeName = Object.prototype.toString.call(val).slice(8, -1);
  return typeName === type || typeName.toLowerCase() === type;
}

export function isObject(val: unknown): val is Record<any, any> {
  return isType<Record<any, any>>(val, 'object');
}
