import type { ElMessageBoxOptions } from 'element-plus';

export interface CustomMessageOptions extends ElMessageBoxOptions {
  confirm?: () => Promise<any>;
}
