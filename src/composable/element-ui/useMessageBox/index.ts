import { ElMessageBox } from 'element-plus';
import type { CustomMessageOptions } from './type';

function defaultOptions(): CustomMessageOptions {
  return {
    title: '提示',
    showCancelButton: true,
    cancelButtonText: '取消',
    showConfirmButton: true,
    confirmButtonText: '确定',
    lockScroll: false,
    closeOnClickModal: false,
    closeOnPressEscape: false,
    draggable: true,
  };
}

export default function useModal() {
  return function (options: CustomMessageOptions) {
    const messageOptions = {
      ...defaultOptions(),
      ...options,
    };
    ElMessageBox({
      ...messageOptions,
      beforeClose: async (action, instance, done) => {
        if (action === 'confirm') {
          if (typeof options.confirm === 'function') {
            instance.confirmButtonLoading = true;
            instance.confirmButtonText = 'Loading...';
            instance.showCancelButton = false;
            try {
              await options.confirm();
              done();
            } finally {
              instance.showCancelButton = true;
              instance.confirmButtonLoading = false;
              instance.confirmButtonText = options.confirmButtonText;
            }
          }
        } else {
          done();
        }
      },
    }).then((action) => {
      console.log(action, 'liuyang');
      // ElMessage({
      //   type: 'info',
      //   message: `action: ${action}`,
      // });
    });
  };
}
