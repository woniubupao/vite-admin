import type { ExtractPropTypes, Ref, SetupContext } from 'vue';
import type { FormInstance } from 'element-plus';
import type { defaultEmits, defaultProps } from './index';

interface FormRefConf { ref?: Ref<FormInstance | undefined>; reset: () => void; type?: 'form' | 'custom' }
export interface UseDialogOptions {
  targetRef: Ref<{
    root: {
      visible: boolean;
      dialogContentRef: {
        $el: HTMLElement;
        [P: string]: any;
      };
    };
  }>;
  resetRefs: FormRefConf[];
  closeWhenOpenErrorOccur?: boolean;
  emits: SetupContext<typeof defaultEmits>['emit'];
  props: ExtractPropTypes<typeof defaultProps>;
  open?: () => void;
  submit?: () => void;
  cancel?: () => void;
}
