import { isRef, nextTick } from 'vue';
import { ElLoading } from 'element-plus';
import type { DialogProps } from 'element-plus';
import type { PropType } from 'vue';
import type { UseDialogOptions } from './type';

export const defaultEmits = {
  // eslint-disable-next-line unused-imports/no-unused-vars
  'update:modelValue': (state: boolean) => true,
  // eslint-disable-next-line unused-imports/no-unused-vars
  'success': (result: any, props: any) => true,
  // eslint-disable-next-line unused-imports/no-unused-vars
  'close': (result: any, props: any) => true,
};

export function defaultProps() {
  return {
    modelValue: { type: Boolean, default: false },
    title: { type: String, default: '标题' },
    beforeClose: { type: Function as PropType<DialogProps['beforeClose']> },
  };
}

export default function useDialog(options: UseDialogOptions) {
  let loadingInstance: ReturnType<typeof ElLoading.service> | null = null;
  function setLoading(tagger: boolean) {
    if (!isRef(options.targetRef)) {
      console.warn('useDialog: warn! options.targetRef is not Ref');
      return;
    }

    if (tagger) {
      loadingInstance = ElLoading.service({
        target: options.targetRef?.value?.root.dialogContentRef.$el,
      });
      return;
    }

    if (!loadingInstance) {
      return;
    }
    loadingInstance.close();
    loadingInstance = null;
  }
  // 重置表单数据
  const resetForm = () => {
    if (!options.resetRefs) {
      return;
    }
    options.resetRefs.forEach((item) => {
      if (item.type !== 'custom' && item.ref && item.ref.value) {
        void nextTick(item.ref.value.resetFields);
      }
      if (typeof item.reset === 'function') {
        item.reset();
      }
      if (item.type !== 'custom' && item.ref && item.ref.value) {
        void nextTick(item.ref.value.clearValidate);
      }
    });
  };
  async function _close() {
    await nextTick();
    options.emits('update:modelValue', false);
  }
  // 弹窗打开
  async function onOpen() {
    if (typeof options.open !== 'function') {
      return;
    }

    try {
      await options.open();
    } catch (e) {
      if (options.closeWhenOpenErrorOccur) {
        onClose();
      }
      throw e;
    }
  }
  // 弹窗关闭
  function onClose() {
    _close();
    options.emits('close', false, options.props);
  }
  // 关闭事件回调
  const onClosed = async () => {
    setLoading(false);
    resetForm();
  };
  async function confirm() {
    if (typeof options.submit !== 'function') {
      _close();
      options.emits('success', false, options.props);
      return;
    }
    if (loadingInstance) {
      return;
    }
    try {
      setLoading(true);
      const result = await Promise.resolve(options.submit());
      _close();
      options.emits('success', result, options.props);
    } catch (e) {
      console.error(e);
      throw e;
    } finally {
      setLoading(false);
    }
  }
  async function cancel() {
    if (typeof options.cancel === 'function') {
      await options.cancel();
    }
    onClose();
  }
  return {
    onConfirm: confirm,
    onCancel: cancel,
    onClose,
    onClosed,
    onOpen,
    setLoading,
  };
}
