import useModal from './element-ui/useMessageBox';
import useDialog, { defaultEmits, defaultProps } from './element-ui/useDialog';

export {
  useModal,
  useDialog,
  defaultEmits,
  defaultProps,
};
