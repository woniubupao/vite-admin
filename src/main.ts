import { createApp } from 'vue';
import App from './App.vue';
import { router } from './router';
import pinia from '@/store';

import { registerElementComponent } from '@/libs/element-plus';
import { registerVxeTableComponent } from '@/libs/vxe-table';
import { registerUniversalDirectives } from '@/directives';
import { registerUniComponent } from '@/components';
import './styles/index.scss';

const app = createApp(App);
app.use(router);
app.use(pinia);
app.use(registerElementComponent);
app.use(registerVxeTableComponent);
app.use(registerUniComponent);
app.use(registerUniversalDirectives);
app.mount('#app');
