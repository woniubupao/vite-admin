import type { ListPageParam } from '@/api/type';
import { $service } from '@/libs/request';

export interface ListParams extends ListPageParam {
  un: string;
}
export interface ListData {
  // ID
  id: string;
  // 账号
  un: string;
  // 所属权限组
  groupname: string;
  // 创建人
  addname: string;
  // 创建时间
  createtime: string;
  // 最后登录时间
  logintime: string;
  // 备注
  remark: string;
  // 可绑定或解绑会员 1/可绑定会员 2/可解绑会员
  switch_bind: 1 | 2;
  // 是否可删除 0/否 1/可删除
  can_delete: 0 | 1;
}
/**
 * 获取管理员列表
 * @link http://apidoc.brohey.cn:8089/project/12/interface/api/24
 * @param data
 */
export function list(data: ListParams) {
  return $service<{ list: ListData[] }>('/user/index', { method: 'GET', searchParams: { ...data } });
}

interface GroupList {
  // ID，当ID==0是，代表手动选择权限
  id: number;
  // 权限组名称
  name: string;
}
interface StoreList {
  // 门店ID
  id: 1;
  // 店名
  name: string;
}
export interface Menudata {
  // ID
  id: string;
  // 权限节点
  authdata: string;
  // 标题
  title: string;
  child: Menudata[];
}
export interface GetAddData {
  // 权限组列表
  groupList: GroupList[];
  // 手动选择权限时-门店列表
  mendianlist: StoreList[];
  // 手动选择权限时-是否显示首页数据统计可选那一行 0/不显示 1/显示
  thisuser_showtj: 0 | 1;
  // 权限设置
  menudata: Menudata[];
}
/**
 * 添加管理员时获取表单数据
 * @link http://apidoc.brohey.cn:8089/project/12/interface/api/52
 */
export function getAddData() {
  return $service('/user/create', { method: 'GET' });
}

/**
 * 编辑管理员时获取表单数据
 * @link http://apidoc.brohey.cn:8089/project/12/interface/api/60
 * @param id
 */
export function getEditData(id: string) {
  return $service<SetAdd>('/user/edit', { method: 'GET', searchParams: { id } });
}

export interface SetAdd {
  info: {
    id?: string;
    // 登录账号
    un: string;
    // 登录密码
    pwd: string;
    // 会员ID
    mid: string;
    // 备注
    remark: string;
    // 权限组ID
    groupid: string;
    // 手动选择权限时-门店ID
    mdid: string;
    // 手动选择权限时-是否显示首页数据统计 0/不显示 1/显示
    showtj: string;
  };
  // 手动选择权限时-选中的权限节点，来自menudata中的authdata字段
  auth_data: string[];
}
export function setAdd(data: SetAdd) {
  return $service('/user/save', { method: 'POST', json: data, showSuccessToast: true });
}

/**
 * 编辑管理员
 * @link http://apidoc.brohey.cn:8089/project/12/interface/api/64
 * @param data
 */
export function setEdit(data: SetAdd) {
  return $service('/user/update', { method: 'POST', json: data, showSuccessToast: true });
}

export function remove(id: string) {
  return $service('/user/del', { method: 'POST', json: { id }, showSuccessToast: true });
}
