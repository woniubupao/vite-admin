import { $service } from '@/libs/request';

export interface LoginFrom {
  username: string;
  password: string;
  // 验证码
  captcha: string;
}

export interface LoginResult {
  token: string;
  user: {
    username: string;
  };
}
/**
 * 登录
 * @param data
 */
export function login(data: LoginFrom) {
  return $service<LoginResult>('/login/index', { method: 'POST', json: data, showSuccessToast: true });
}

// 退出登录
export function logout() {
  return $service('/logout', { method: 'POST', showSuccessToast: true });
}

export interface MenuItem {
  // ID
  id: string;
  // 标题
  title: string;
  // 前端标识
  front_name: string;
  child: MenuItem[];
}
// 获取菜单
export function getMenu() {
  return $service<{ list: MenuItem[] }>('/admin/menu', { method: 'GET' });
}
