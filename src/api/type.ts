export interface ListPageParam {
  page: string | number;
  limit: string | number;
}
