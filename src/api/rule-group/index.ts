import type { ListPageParam } from '@/api/type';
import { $service } from '@/libs/request';
import type { GetAddData } from '@/api/admin';

export interface ListParams extends ListPageParam {
  name: string;
}
export interface ListData {
  // 权限组ID
  id: string;
  // 权限组名称
  name: string;
  // 权重
  sort: string;
  // 创建时间
  createtime: string;
  // 备注
  remark: string;
}
export function list(data: ListParams) {
  return $service<{ list: ListData[] }>('/user_group/index', { method: 'GET', searchParams: { ...data } });
}

type GetAddResponse = Omit<GetAddData, 'groupList'>;
export function getAdd() {
  return $service<GetAddResponse>('/usergroup/create', { method: 'GET' });
}

export interface SetAdd {
  info: {
    // 权限组名称
    name: string;
    // 权重
    sort: string;
    // 备注
    remark: string;
    // 手动选择权限时-门店ID
    mdid: string;
    // 手动选择权限时-是否显示首页数据统计 0/不显示 1/显示
    showtj: string;
  };
  // 手动选择权限时-选中的权限节点，来自menudata中的authdata字段
  auth_data: string[];
}
// 添加权限组
export function setAdd(data: SetAdd) {
  return $service('/user_group/save', { method: 'POST', json: data, showSuccessToast: true });
}
/**
 * 编辑权限组
 * @link http://apidoc.brohey.cn:8089/project/12/interface/api/44
 * @param data
 */
export function setEdit(data: SetAdd) {
  return $service('/user_group/update', { method: 'POST', json: data, showSuccessToast: true });
}

/**
 * 编辑权限组时获取数据
 * @link http://apidoc.brohey.cn:8089/project/12/interface/api/40
 * @param id
 */
export function getEditData(id: string) {
  return $service('/user_group/edit', { method: 'GET', searchParams: { id } });
}

/**
 * 删除权限组
 * @link http://apidoc.brohey.cn:8089/project/12/interface/api/48
 * @param id
 */
export function remove(id: string) {
  return $service('/user_group/del', { method: 'POST', json: { id }, showSuccessToast: true });
}
