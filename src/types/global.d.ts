// https://cn.vuejs.org/guide/extras/web-components.html#web-components-and-typescript
declare module 'vue' {
    export interface GlobalComponents {
        UniTable: typeof import('@/components/index')['UniTable'],
        UniDialog: typeof import('@/components/index')['UniDialog'],
        UniFormItem: typeof import('@/components/index')['UniFormItem'],
        UniLayout: typeof import('@/components/index')['UniLayout'],
        UniImage: typeof import('@/components/index')['UniImage'],
        UniDivider: typeof import('@/components/index')['UniDivider'],
    }
}

// why need this?
// more to see: https://vuejs.org/guide/typescript/options-api.html#augmenting-global-properties
export {}