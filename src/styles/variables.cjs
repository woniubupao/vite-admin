const basicColors = {
  primary: '#3f8cff',
  warning: '#f89e1b',
  success: '#20c18b',
  danger: '#ee5954',
  error: '#ee5954',
  info: '#3872f7',
};

const textColors = {
  primary: '#050505',
  main: '#555',
  helper: '#a0a0a0',
};

const grayColors = {
  100: '#f0f0f0',
  200: '#eeeeee',
  300: '#dddddd',
};

module.exports = {
  basicColors,
  textColors,
  grayColors,
};
