import { ky, withKyWrapper } from './ky';

import { afterResponseHook, beforeErrorHook, beforeRequestHook } from './hooks';
import type { APIResponsePayload, FetchOptions } from './type';
import { isObject } from '@/utils';
import { HTTP_DATA_SERVICE_URL } from '@/constants/service';

const $ky = ky.create({
  prefixUrl: HTTP_DATA_SERVICE_URL,
  timeout: 1000 * 60,
  retry: 0,
  hooks: {
    beforeRequest: [beforeRequestHook],
    afterResponse: [afterResponseHook],
    beforeError: [beforeErrorHook],
  },
});

export const request = withKyWrapper<FetchOptions>($ky);

export function $request<T>(
  url: string | URL,
  options?: FetchOptions<APIResponsePayload<T>>,
) {
  return request(url, { ...options });
}

function filterNullableValue(data: Record<any, any>) {
  return Object.keys(data)
    .reduce((result, key) => {
      const value = data[key];
      if (value !== null && value !== undefined) {
        result[key] = value;
      }
      return result;
    }, {} as Record<any, any>);
}

export function $service<T = any>(
  url: string | URL,
  options?: FetchOptions<APIResponsePayload<T>>,
) {
  if (options?.searchParams && isObject(options.searchParams)) {
    options.searchParams = filterNullableValue(options.searchParams);
  }
  if (options?.json && isObject(options.json)) {
    options.json = filterNullableValue(options.json);
  }
  return $request(url, options)
    .json<APIResponsePayload<T>>()
    .then(resp => resp.data);
}
