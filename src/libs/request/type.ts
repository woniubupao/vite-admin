import type { NormalizedOptions, Options } from './ky';

type CallbackOptionFn<R, P = any> = (request: Request, response: Response, payload?: P) => R;

interface CustomOptions<P = any> {
  showErrorToast?: boolean | CallbackOptionFn<boolean, P>;
  showSuccessToast?: boolean | CallbackOptionFn<boolean, P>;
}

export interface FetchOptions<P = any> extends Options, CustomOptions<P> {
  method?: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'HEAD' | 'DELETE';
}

export interface NormalizedFetchOptions<P = any> extends NormalizedOptions, CustomOptions<P> {}

export interface APIResponsePayload<T> {
  code: number;
  data: T;
  info: string;
}
