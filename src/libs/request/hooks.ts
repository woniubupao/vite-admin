import { ElMessage } from 'element-plus';
import type { BeforeErrorHook, BeforeRequestHook } from './ky';
import { servicePayloadErrorHandler } from './ky';
import type { APIResponsePayload, NormalizedFetchOptions } from './type';
import store from '@/store';
import userStore from '@/store/module/user';
import {
  HTTP_API_AUTH_HEADER_KEY,
  HTTP_API_REQUESTED_VUE,
  HTTP_API_REQUESTED_WITH,
  sessionHasExpiredError,
} from '@/constants/service';
import { router } from '@/router';

const userStoreHook = userStore(store);

export const beforeRequestHook: BeforeRequestHook = (request) => {
  // 请求拦截器
  const token = userStoreHook.token;
  if (token) {
    request.headers.set(HTTP_API_AUTH_HEADER_KEY, `${token}`);
  }
  request.headers.set(HTTP_API_REQUESTED_WITH, 'XMLHttpRequest');
  request.headers.set(HTTP_API_REQUESTED_VUE, '1');
  return request;
};

function getToastEnableConfig(
  type: 'success' | 'error',
  request: Request,
  options: NormalizedFetchOptions,
  response: Response,
  payload?: any,
) {
  const config = type === 'success' ? options.showSuccessToast : options.showErrorToast;
  if (typeof config === 'boolean') {
    return config;
  }
  if (typeof config === 'function') {
    return !!config(request, response, payload);
  }
  return type === 'error';
}

export const afterResponseHook = servicePayloadErrorHandler<
  NormalizedFetchOptions,
  APIResponsePayload<unknown>
>(
  (request, options, response, payload) => {
    // 响应拦截器
    if (payload.code === 0) {
      if (getToastEnableConfig('success', request, options, response, payload)) {
        ElMessage({ type: 'success', message: payload.info || '操作成功', grouping: true });
      }
      return { hasError: false };
    }

    if (payload.code === sessionHasExpiredError) {
      userStoreHook.clear();
      router.replace({ name: 'Login' });
    }

    if (getToastEnableConfig('error', request, options, response, payload)) {
      ElMessage({ type: 'error', message: payload.info || '操作失败', grouping: true });
    }

    return { hasError: true, reason: 'payload code is not in the correct range' };
  },
);

export const beforeErrorHook: BeforeErrorHook = (error) => {
  if (!getToastEnableConfig('error', error.request, error.options, error.response)) {
    return error;
  }

  let message: string;
  switch (error.response.status) {
    case 400:
      message = '请求错误';
      break;
    case 401:
      message = '未授权，请登录';
      break;
    case 403:
      message = '拒绝访问';
      break;
    case 404:
      message = `请求地址出错: ${new URL(error.request.url).pathname}`;
      break;
    case 408:
      message = '请求超时';
      break;
    case 500:
      message = '服务器内部错误';
      break;
    case 501:
      message = '服务未实现';
      break;
    case 502:
      message = '网关错误';
      break;
    case 503:
      message = '服务不可用';
      break;
    case 504:
      message = '网关超时';
      break;
    case 505:
      message = 'HTTP版本不受支持';
      break;
    default:
      message = '请求失败';
      break;
  }

  if (!message) {
    return error;
  }

  const options = error.options as NormalizedFetchOptions;
  if (options.showErrorToast !== false) {
    ElMessage({ type: 'error', message, grouping: true });
  }

  return error;
};
