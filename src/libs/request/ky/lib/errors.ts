import type { NormalizedOptions } from 'ky';

export class ServiceError<O extends NormalizedOptions> extends Error {
  public request: Request;
  public options: O;
  public response: Response;
  public responseBody: any;
  public error?: Error;

  constructor(
    reason: string,
    request: Request,
    options: O,
    response: Response,
    error?: Error,
    responseBody?: any,
  ) {
    super(`Request failed with ${reason || 'internal service error'}`);

    this.name = 'ServiceError';
    this.request = request;
    this.options = options;
    this.response = response;
    this.error = error;
    this.responseBody = responseBody;
  }
}
