import type { AfterResponseHook, NormalizedOptions } from 'ky';
import { ServiceError } from './errors';

import type { ServicePayloadErrorChecker } from './types';

export function servicePayloadErrorHandler<
  O extends NormalizedOptions = NormalizedOptions, P = any,
>(hasServiceError: ServicePayloadErrorChecker<O, P>): AfterResponseHook {
  return async (request, options, response) => {
    // check http state first
    if (!response.ok) {
      return response;
    }

    // not json format response, skip response payload check
    if (request.headers.get('accept')?.toLowerCase() !== 'application/json') {
      return response;
    }

    // parse response to json format
    let payload: P;
    try {
      payload = await response.clone().json();
    } catch (error: any) {
      throw new ServiceError(
        'error occurred while parsing the response content',
        request,
        options,
        response,
        error,
      );
    }

    // check payload
    let hasError: boolean;
    let reason: string | undefined;
    try {
      const result = await hasServiceError(request, options as O, response, payload);
      hasError = result.hasError;
      reason = result.reason;
    } catch (error: any) {
      throw new ServiceError(
        'error occurred while detecting the response content',
        request,
        options,
        response,
        error,
        payload,
      );
    }

    if (!hasError) {
      return response;
    }

    throw new ServiceError(
      reason || 'error in service api response',
      request,
      options,
      response,
      undefined,
      payload,
    );
  };
}
