import type { Options } from 'ky';
import type { Input, Ky } from './types';

const isHTTPLink = (url: string) => /^https?:\/\//.test(url);

function $fetch<T extends Options>(instance: Ky, url: string | URL, options?: T) {
  if (typeof url === 'string') {
    // ky is not allow to use [/xxx] with prefixUrl
    url = url.replace(/^\//, '');
    // converts a full url string to a URL instance to avoid the concatenation of prefixUrl
    if (isHTTPLink(url)) {
      url = new URL(url);
    }
  }
  return instance(url, options);
}

export function withKyWrapper<O extends Options>(ky: Ky) {
  return (url: Input, options?: O) => $fetch(ky, url, options);
}
