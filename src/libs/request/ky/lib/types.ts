import type ky from 'ky';
import type { NormalizedOptions } from 'ky';

export type Ky = typeof ky;

export type Input = URL | string;

export interface ServicePayloadErrorCheckResult {
  hasError: boolean;
  reason?: string;
}

export type ServicePayloadErrorChecker<O extends NormalizedOptions, P = any> = (
  request: Request,
  options: O,
  response: Response,
  payload: P,
) => ServicePayloadErrorCheckResult | Promise<ServicePayloadErrorCheckResult>;
