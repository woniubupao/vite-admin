import type { App, Plugin } from 'vue';
import { Colgroup, Column, Table } from 'vxe-table';
import 'vxe-table/lib/style.css';

export const registerVxeTableComponent: Plugin = (app: App) => {
  app.use(Table);
  app.use(Column);
  app.use(Colgroup);
};
