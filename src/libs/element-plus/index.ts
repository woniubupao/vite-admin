import type { App, Plugin } from 'vue';
import {
  ElButton,
  ElCheckbox,
  ElConfigProvider,
  ElDropdown,
  ElDropdownItem,
  ElDropdownMenu,
  ElForm,
  ElFormItem,
  ElImage,
  ElInput,
  ElLink,
  ElLoading,
  ElPagination,
  ElRadio,
  ElRadioGroup,
  ElScrollbar, ElSelect,
} from 'element-plus';

export const registerElementComponent: Plugin = (app: App) => {
  app.directive('loading', ElLoading.directive);
  app.use(ElConfigProvider);
  app.use(ElButton);
  app.use(ElLink);
  app.use(ElCheckbox);
  app.use(ElPagination);
  app.use(ElForm);
  app.use(ElFormItem);
  app.use(ElInput);
  app.use(ElSelect);
  app.use(ElScrollbar);
  app.use(ElRadioGroup);
  app.use(ElRadio);
  app.use(ElImage);
  app.use(ElDropdown);
  app.use(ElDropdownItem);
  app.use(ElDropdownMenu);
};
