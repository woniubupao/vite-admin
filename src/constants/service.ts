/**
 * SessionToken 的 HTTP Header 参数字段名称
 */
export const HTTP_API_AUTH_HEADER_KEY = 'X-ACCESS-TOKEN';

/**
 * version 的 HTTP Header 参数字段名称
 */
export const HTTP_API_REQUESTED_WITH = 'X-Requested-With';

/**
 * CompanyId 的 HTTP Header 参数字段名称
 */
export const HTTP_API_REQUESTED_VUE = 'X-Requested-VUE';

/**
 * 鉴权 API 服务 Url
 */
export const HTTP_AUTH_SERVICE_URL = import.meta.env.VITE_HTTP_AUTH_SERVICE_URL || '/auth-api';

/**
 * 业务 API 服务 Url
 */
export const HTTP_DATA_SERVICE_URL = import.meta.env.VITE_HTTP_DATA_SERVICE_URL || '/api';

/**
 * WebSocket 服务 Url
 */
export const WEB_SOCKET_SERVICE_URL = import.meta.env.VITE_WEB_SOCKET_SERVICE_URL || '';

/**
 * 缺少安全码
 */
export const missSecurityCodeError = 1003;

/**
 * 登录已过期
 */
export const sessionHasExpiredError = 1001;
