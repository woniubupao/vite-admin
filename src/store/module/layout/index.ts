import { defineStore } from 'pinia';
import type { LayoutState } from './type';
import type { AuthMenu } from '@/components/uni-components/uni-layout/uni-menu/type';

const scopeMenuKey = 'menu';
function defaultLayoutState(): LayoutState {
  return {
    menu: {
      collapse: false,
      currentChildrenMenu: [],
      menuData: [
        {
          id: '1',
          title: '菜单1',
          name: 'Test',
          icon: 'icon-s-goods',
        },
        {
          id: '2',
          title: '菜单2',
          icon: 'icon-s-goods',
          children: [
            { id: '2-1', title: '菜单2-1', icon: 'icon-s-goods', name: 'GoodsList' },
            { id: '2-2', title: '菜单2-2', icon: 'icon-s-goods' },
          ],
        },
      ],
    },
  };
}
export const useLayoutStore = defineStore(String(Symbol('layout')), {
  state: () => defaultLayoutState(),
  getters: {
    collapse: state => state.menu.collapse,
    menuData: state => state.menu.menuData,
    currentChildrenMenu: state => state.menu.currentChildrenMenu,
  },
  actions: {
    init() {
      const menu = localStorage.getItem(`${scopeMenuKey}_menuData`);
      this.menu.menuData = menu ? JSON.parse(menu) : [];
    },
    clear() {
      localStorage.removeItem(`${scopeMenuKey}_menuData`);
    },
    setMenuData(menu: AuthMenu[]) {
      this.menu.menuData = menu;
      localStorage.setItem(`${scopeMenuKey}_menuData`, JSON.stringify(menu));
    },
    changeCollapse() {
      this.menu.collapse = !this.menu.collapse;
    },
    changeCurrentChildrenMenu(data: LayoutState['menu']['currentChildrenMenu']) {
      this.menu.currentChildrenMenu = data;
    },
  },
});
