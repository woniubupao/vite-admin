import type { AuthMenu } from '@/components/uni-components/uni-layout/uni-menu/type';

interface LayoutStateMenu {
  collapse: boolean;
  currentChildrenMenu: AuthMenu[];
  menuData: AuthMenu[];
}
export interface LayoutState {
  menu: LayoutStateMenu;
}
