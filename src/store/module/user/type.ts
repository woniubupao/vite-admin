export type AccessToken = string;
export type TokenType = string;
export interface UserInfo {
  username: string;
}

export interface UserState {
  access_token: AccessToken;
  token_type: TokenType;
  user_info: UserInfo;
}

export interface SetStatusOption {
  cache: boolean;
  plain: boolean;
}
