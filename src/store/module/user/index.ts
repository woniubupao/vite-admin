import { defineStore } from 'pinia';
import type { UserState } from './type';

const scopeKey = 'user';
function defaultState(): UserState {
  return {
    access_token: '',
    token_type: '',
    user_info: {
      username: 'admin',
    },
  };
}
export default defineStore(String(Symbol('userInfo')), {
  state: () => defaultState(),
  getters: {
    token: state => state.access_token,
    tokenPrefix: state => state.token_type,
    userInfo: state => state.user_info,
  },
  actions: {
    setState<T extends keyof UserState>(key: T, data: UserState[T], cache = false) {
      this[key] = data;
      if (cache) {
        localStorage.setItem(`${scopeKey}_${key}`, JSON.stringify(data));
      }
    },
    clear() {
      localStorage.removeItem(`${scopeKey}_access_token`);
      localStorage.removeItem(`${scopeKey}_token_type`);
      localStorage.removeItem(`${scopeKey}_user_info`);
    },
    init() {
      const token = localStorage.getItem(`${scopeKey}_access_token`);
      const tokenPrefix = localStorage.getItem(`${scopeKey}_token_type`);
      const userInfo = localStorage.getItem(`${scopeKey}_user_info`);
      this.setState('access_token', JSON.parse(token));
      this.setState('token_type', JSON.parse(tokenPrefix));
      this.setState('user_info', JSON.parse(userInfo));
    },
  },
});
