import { defineComponent, h } from 'vue';
import type { ExtractPropTypes } from 'vue';

const props = {
  margin: {
    type: String,
  },
};
type UniDividerProps = ExtractPropTypes<typeof props>;
export const UniDivider = defineComponent({
  name: 'UniDivider',
  setup(props: UniDividerProps) {
    return () => h('div', {
      class: `w-full h-[1px] bg-gray-200 my-[${props.margin || '0px'}]`,
    });
  },
});
