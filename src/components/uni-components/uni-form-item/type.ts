import type { RuleItem } from 'async-validator';

export type ValidateTrigger = 'blur' | 'change';

export interface ValidateRule extends RuleItem {
  trigger?: ValidateTrigger;
}

export interface ValidateOptions {
  errorMessage: string;
  emptyMessage: string;
  number: boolean;
}

export interface ValidateRuleConfig<T> {
  isEnable?: (value?: T) => boolean;
  createRules: (config: ValidateOptions, value?: T) => ValidateRule | ValidateRule[];
}
