import type { ValidateOptions, ValidateRuleConfig } from './type';

export function withRules<T>(config: ValidateOptions, rule: ValidateRuleConfig<T>, value?: T) {
  const rules = rule.isEnable?.(value) === true
    ? rule.createRules(config, value)
    : [];
  return Array.isArray(rules) ? rules : [rules];
}

export const notFalse = (value: any) => value !== false;
