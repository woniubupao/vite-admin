import type { RuleItem } from 'async-validator';
import { notFalse } from './utils';

import type { ValidateRuleConfig } from './type';
import { isNullable } from '@/utils';

/**
 * 必填校验
 */
export const $required: ValidateRuleConfig<boolean> = {
  isEnable: notFalse,
  createRules: config => ({
    required: true,
    message: config.emptyMessage || config.errorMessage,
  }),
};

/**
 * 手机号码校验（仅国内）
 */
export const $mobile: ValidateRuleConfig<boolean> = {
  isEnable: notFalse,
  createRules: config => ({
    trigger: 'blur',
    pattern: /^1[0-9]{10}$/,
    message: config.errorMessage || '手机号码格式错误',
  }),
};

/**
 * 座机号码校验（仅国内）
 */
export const $phone: ValidateRuleConfig<boolean> = {
  isEnable: notFalse,
  createRules: config => ({
    pattern: /0\d{2,3}-\d{7,8}|\(?0\d{2,3}[)-]?\d{7,8}|\(?0\d{2,3}[)-]*\d{7,8}/,
    message: config.errorMessage || '手机号码格式错误',
  }),
};

/**
 * 仅字母
 */
export const $alphabet: ValidateRuleConfig<boolean> = {
  isEnable: notFalse,
  createRules: config => ({
    pattern: /^[a-zA-Z]*$/,
    message: config.errorMessage || '请输入字母',
  }),
};

/**
 * 长度校验
 */
export const $length: ValidateRuleConfig<number> = {
  isEnable: value => !!value && value > 0,
  createRules: (config, value) => ({
    len: value,
    message: config.errorMessage || `请输入${value}个字符`,
  }),
};

/**
 * 输入数字
 */
export const $number: ValidateRuleConfig<boolean> = {
  isEnable: notFalse,
  createRules: config => ({
    pattern: /^[-+]?\d+(.?\d+)?$/,
    message: config.errorMessage || '请输入数字',
  }),
};
// 校验数字要小于某数
export const $lt: ValidateRuleConfig<number> = {
  isEnable: val => !isNullable(val),
  createRules: (config, val) => {
    return {
      type: 'number',
      asyncValidator: (rule: any, value: any, callback: (e?: Error) => void) => {
        if (!isNullable(val) && Number(value) >= val) {
          return callback(new Error(config.errorMessage || `该项值需小于${val}`));
        }
        callback();
      },
    };
  },
};

// 校验数字要小于等于某数
export const $lte: ValidateRuleConfig<number> = {
  isEnable: val => !isNullable(val),
  createRules: (config, val) => {
    return {
      type: 'number',
      asyncValidator: (rule: any, value: any, callback: (e?: Error) => void) => {
        if (!isNullable(val) && Number(value) > val) {
          return callback(new Error(config.errorMessage || `该项值不能大于${val}`));
        }
        callback();
      },
    };
  },
};

// 校验数字要大于某数
export const $gt: ValidateRuleConfig<number> = {
  isEnable: val => !isNullable(val),
  createRules: (config, val) => {
    return {
      type: 'number',
      asyncValidator: (rule: any, value: any, callback: (e?: Error) => void) => {
        if (!isNullable(val) && Number(value) <= val) {
          return callback(new Error(config.errorMessage || `该项值需大于${val}`));
        }
        callback();
      },
    };
  },
};

// 校验数字要大于等于某数
export const $gte: ValidateRuleConfig<number> = {
  isEnable: val => !isNullable(val),
  createRules: (config, val) => {
    return {
      type: 'number',
      asyncValidator: (rule: any, value: any, callback: (e?: Error) => void) => {
        if (!isNullable(val) && Number(value) < val) {
          return callback(new Error(config.errorMessage || `该项值不能小于${val}`));
        }
        callback();
      },
    };
  },
};

// 输入整数
export const $int: ValidateRuleConfig<boolean> = {
  isEnable: notFalse,
  createRules: config => ({
    pattern: /^-?[1-9]\d*$/,
    message: config.errorMessage || '请输入整数',
  }),
};

// 输入正整数
export const $intz: ValidateRuleConfig<boolean> = {
  isEnable: notFalse,
  createRules: config => ({
    pattern: /^[1-9]\d*$/,
    message: config.errorMessage || '请输入正整数',
  }),
};

/**
 * 校验最大输入/数组长度/值
 */
export const $max: ValidateRuleConfig<number> = {
  isEnable: val => !isNullable(val),
  createRules: (config, max = 0) => {
    if (config.number) {
      return {
        type: 'number',
        asyncValidator: (rule: any, value: any, callback: (e?: Error) => void) => {
          if (Number(value) > max) {
            callback(new Error(config.errorMessage || `该项值不能大于${max}`));
          }
          callback();
        },
      };
    } else {
      return {
        asyncValidator: (rule: any, value: any, callback: (e?: Error) => void) => {
          if (Array.isArray(value) && value.length > max) {
            callback(new Error(config.errorMessage || `最多选择${max}项`));
          } else if (value.length > max) {
            callback(new Error(config.errorMessage || `最多可输入${max}个字符`));
          }
          callback();
        },
      };
    }
  },
};
/**
 * 校验最少输入/数组长度/值
 */
export const $min: ValidateRuleConfig<number> = {
  isEnable: val => !isNullable(val),
  createRules: (config, min = 0) => {
    if (config.number) {
      return {
        type: 'number',
        asyncValidator: (rule: any, value: any, callback: (e?: Error) => void) => {
          if (Number(value) < min) {
            return callback(new Error(config.errorMessage || `该项值不能小于${min}`));
          }
          callback();
        },
      };
    } else {
      return {
        asyncValidator: (rule: any, value: any, callback: (e?: Error) => void) => {
          if (Array.isArray(value) && value.length < min) {
            return callback(new Error(config.errorMessage || `至少选择${min}项`));
          } else if (value.length < min) {
            return callback(new Error(config.errorMessage || `不少于${min}个字符`));
          }
          callback();
        },
      };
    }
  },
};
/**
 * 校验身份证
 */

export const idCard
= /^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;

export const $idCard: ValidateRuleConfig<boolean> = {
  isEnable: notFalse,
  createRules: config => ({
    trigger: 'blur',
    pattern: idCard,
    message: config.errorMessage || '请输入身份证号',
  }),
};

/**
 * 港澳通行证
 * 规则： H/M + 10位或6位数字
 * 样本： H1234567890
 */
export const gaCard = /^([A-Z]\d{6,10}(\(\w{1}\))?)$/;
export const $gaCard: ValidateRuleConfig<boolean> = {
  isEnable: notFalse,
  createRules: config => ({
    pattern: gaCard,
    message: config.errorMessage || '请输入港澳通行证',
  }),
};

/**
 * 台湾来往大陆通行证
 * 规则： 新版8位或18位数字， 旧版10位数字 + 英文字母
 * 样本： 12345678 或 1234567890B
 */
export const twCard = /^\d{8}|^[a-zA-Z0-9]{10}|^\d{18}$/;
export const $twCard: ValidateRuleConfig<boolean> = {
  isEnable: notFalse,
  createRules: config => ({
    trigger: 'blur',
    pattern: twCard,
    message: config.errorMessage || '请输入台胞证',
  }),
};

/**
 * 护照
 * 规则： 14/15开头 + 7位数字, G + 8位数字, P + 7位数字, S/D + 7或8位数字,等
 * 样本： 141234567, G12345678, P1234567
 */
export const hzCard = /^([a-zA-z]|[0-9]){5,17}$/;
export const $hzCard: ValidateRuleConfig<boolean> = {
  isEnable: notFalse,
  createRules: config => ({
    trigger: 'blur',
    pattern: hzCard,
    message: config.errorMessage || '请输入护照',
  }),
};

/**
 * 邮箱份证
 */
export const $email: ValidateRuleConfig<boolean> = {
  isEnable: notFalse,
  createRules: config => ({
    trigger: 'blur',
    type: 'email',
    message: config.errorMessage || '邮箱地址格式不正确',
  }),
};

export const $verify: ValidateRuleConfig<RuleItem['asyncValidator']> = {
  isEnable: value => typeof value === 'function',
  createRules: (config, value) => ({
    trigger: 'blur',
    asyncValidator: value,
  }),
};
