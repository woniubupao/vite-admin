export interface AuthMenu {
  id: string;
  title: string;
  icon: string;
  name?: string;
  children?: AuthMenu[];
}

export type OnMenusItemClick = (menu: AuthMenu) => void;
