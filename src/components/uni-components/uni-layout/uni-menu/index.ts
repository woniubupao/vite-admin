import { defineComponent, h } from 'vue';
import type { PropType } from 'vue';
import { ElMenu, ElMenuItem } from 'element-plus';
import { useRouter } from 'vue-router';
import type { AuthMenu, OnMenusItemClick } from './type';
import { useLayoutStore } from '@/store/module/layout';
import store from '@/store';

const useLayoutStoreHooks = useLayoutStore(store);
function createMenuTitleNode(menu: AuthMenu) {
  return [
    h('span', { class: 'flex-1 truncate ml-2.5' }, menu.title),
  ];
}

function createMenuitem(menus: AuthMenu[], onMenusItemClick?: OnMenusItemClick) {
  if (!menus.length) {
    return;
  }
  return menus.map((item) => {
    return h(ElMenuItem, {
      index: item.id,
      onClick: onMenusItemClick ? () => onMenusItemClick?.(item) : undefined,
    }, {
      default: () => item.icon ? h('i', { class: `iconfont ${item.icon}` }) : null,
      title: () => createMenuTitleNode(item),
    });
  });
}

const Menu = defineComponent({
  name: 'MenuLayout',
  props: {
    collapse: {
      type: Boolean,
    },
    menuData: {
      type: Object as PropType<AuthMenu[]>,
    },
    isTop: {
      type: Boolean,
      default: true,
    },
  },
  setup(props) {
    const router = useRouter();
    const onMenusItemClick = (menu: AuthMenu) => {
      if (menu.children && menu.children.length) {
        useLayoutStoreHooks.changeCurrentChildrenMenu(menu.children);
        if (menu.children[0].name) {
          router.push({ name: menu.children[0].name });
        }
        return;
      } else {
        if (props.isTop) {
          useLayoutStoreHooks.changeCurrentChildrenMenu([]);
        }
      }
      if (menu.name) {
        router.push({ name: menu.name });
      }
    };
    return () => {
      return h(ElMenu, {
        ...props,
        class: 'el-menu-vertical_custom',
      }, () => createMenuitem(props.menuData, onMenusItemClick));
    };
  },
});

export default Menu;
