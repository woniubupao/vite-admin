import type { VxeTableProps } from 'vxe-table';

type Fetch<T, U> = (data: T) => Promise<{
  const?: number;
  currentPage: number;
  list: U[];
}>;
export interface UniTableProps<T, U> extends VxeTableProps {
  fetch: Fetch<T, U>;
}

export interface RefreshParams {
  clearPage: boolean;
  clearForm: boolean;
}
