import type { App, Plugin } from 'vue';
import UniTable from './uni-components/uni-table/index.vue';
import UniDialog from './uni-components/uni-dialog/index.vue';
import UniLayout from './uni-components/uni-layout/index.vue';
import UniImage from './uni-components/uni-image/index.vue';
import { UniFormItem } from './uni-components/uni-form-item/index';
import { UniDivider } from './uni-components/uni-divider/index';

export const registerUniComponent: Plugin = (app: App) => {
  app.component(UniTable.name, UniTable);
  app.component(UniDialog.name, UniDialog);
  app.component(UniFormItem.name, UniFormItem);
  app.component(UniLayout.name, UniLayout);
  app.component(UniImage.name, UniImage);
  app.component(UniDivider.name, UniDivider);
};

export { UniTable, UniDialog, UniFormItem, UniLayout, UniImage, UniDivider };
