import type { App } from 'vue';
import { vCopy } from './copy';
import input from './input';

export function registerUniversalDirectives(app: App) {
  // 复制指令
  app.directive('copy', vCopy);
  // 输入限制
  app.directive('input', input);
}

// todo 声明App Vue 指令全局类型
// todo 完善 input 类型 调整即将废弃的 initEvent
