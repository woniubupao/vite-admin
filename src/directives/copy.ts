import { copy } from '@/utils';

function setValue(el: any, value: any) {
  el.dataset.value = value || el.innerText;
}
export const vCopy = {
  mounted: (el: any, binding: any) => {
    setValue(el, binding.value);
    el.addEventListener('click', (e: Event) => {
      e.stopPropagation();
      copy(el.dataset.value);
    });
  },
  updated(el: any, binding: any) {
    setValue(el, binding.value);
  },
};
