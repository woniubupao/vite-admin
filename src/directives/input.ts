function onCompositionStart(e: any) {
  e.target.composing = true;
}

function onCompositionEnd(e: any) {
  if (!e.target.composing) {
    return;
  }
  e.target.composing = false;
  const event = document.createEvent('HTMLEvents');
  event.initEvent('input', true, true);
  e.target.dispatchEvent(event);
}

const rules = {
  /**
   * 字符串处理
   * @param {Number} length - 最大长度
   * @param {Function} replace - 处理函数
   * @returns {function(...[*]=)}
   */
  string({ length, replace }: any = {}) {
    return function (event: any) {
      const target = event.target;
      if (!target || !target.value) {
        return;
      }
      let value = target.value;
      if (target.composing) {
        return;
      }
      if (length && typeof length === 'number') {
        value = value.substring(0, length);
      }
      if (replace && typeof replace === 'function') {
        value = replace(value);
      }
      target.value = value;
    };
  },

  /**
   * 数字内容处理
   * @param {Boolean} plus
   * @param {Boolean} minus 是否允许负数
   * @param {Number} digits 小数位数
   * @param {Number} length 长度
   * @returns {function(...[*]=)}
   */
  number({ plus = true, minus = false, digits = 0, length = 0 }: any = {}) {
    return function (event: any) {
      const target = event.target;
      if (target.composing)
      { return; }

      let value = target.value;

      // 不允许正数
      if (!plus) {
        value = value.replace(/\+/g, '-');
      }
      // 不允许负数
      if (!minus) {
        value = value.replace(/-/g, '');
      }

      // 不允许小数
      if (!digits)
      { value = value.replace(/\./g, ''); }

      value = value
        // 去除 开头的 (+)
        .replace(/^[+]/g, '')
        // 去除整数部分开始位置中的 0
        .replace(/^(-)?0+(\d+)/g, '$1$2')
        // 替换 开头的 . 为 0.
        .replace(/^[.]/g, '0.')
        // 替换 开头的 -. 为 -0.
        .replace(/^-\./g, '-0.')
        // 去除 非 (-, ., 数字) 的内容
        .replace(/[^\-.\d]/g, '')
        // 将 开头的 - 替换为 $!$
        .replace(/^-/, '$!$')
        // 将 第一个 . 替换为 $#$
        .replace('.', '$#$')
        // 去除 所有的 (., -)
        .replace(/[-/.]/g, '')
        // 还原 -
        .replace('$!$', '-')
        // 还原 .
        .replace('$#$', '.')
        // 整合分组
        .replace(new RegExp(`^(-)?(\\d+)\\.(\\d{${digits}}).*$`), '$1$2.$3');

      // 去除多个 0
      // -000000.1

      if (length > 0) {
        value = value.slice(0, length);
      }

      target.value = value;
    };
  },
};

/**
 * enum Arg {
 *   string = 'string',
 *   int = 'int',
 *   float = 'float',
 * }
 *
 * interface Value {
 *   min?: number;
 *   max?: number;
 *   digits?: number;
 *   plus?: boolean;
 *   minus?: boolean;
 *   length?: number;
 *   regexp?: regexp;
 * }
 */

export default function input(el: any, binding: any, vNode: any) {
  const { arg: mode = 'string' } = binding;
  const { plus = true, minus = false } = binding.modifiers;
  const { digits = 2, length, replace } = binding.value || {};

  let handler;

  switch (mode) {
    case 'string':
      handler = rules.string({ length, replace });
      break;
    case 'numberStr':
      handler = rules.string({ replace: (val: any) => val.replace(/\D/g, '') });
      break;
    case 'int':
      handler = rules.number({ digits: 0, plus, minus });
      break;
    case 'mobile':
      handler = rules.number({ digits: 0, plus: true, minus: false, length: 11 });
      break;
    case 'float':
    case 'number':
      handler = rules.number({ digits, plus, minus });
      // 去除小数末尾多个 0
      el.addEventListener('change', (event: any) => {
        const value = event.target.value.replace(/\.0+$/g, '');
        vNode.data?.model?.callback(value);
      }, { capture: true });
      break;
    default:
      throw new TypeError(
          `Directive v-input's arg param '${mode}' is not allowed.
           It only can be 'string', 'int' and 'float'`,
      );
  }

  el.addEventListener('compositionstart', onCompositionStart);
  el.addEventListener('compositionend', onCompositionEnd);
  el.addEventListener('input', handler, { capture: true });
}
