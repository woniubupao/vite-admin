import { createRouter, createWebHashHistory } from 'vue-router';
import dynamic from './dynamic';
import immutable from './static';
import Layout from '@/components/uni-components/uni-layout/index.vue';
import userStore from '@/store/module/user/index';
import { useLayoutStore } from '@/store/module/layout';
import store from '@/store';

const userStoreHook = userStore(store);
const layoutStoreHook = useLayoutStore(store);
const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    // 登录
    {
      name: 'Login',
      path: '/login',
      component: () => import('@/views/login/index.vue'),
    },

    {
      path: '/',
      component: Layout,
      redirect: '/admin',
      children: [
        ...immutable,
        ...dynamic,
      ],
    },
  ],
});
router.beforeEach(async (to) => {
  const { name: toName } = to;
  userStoreHook.init();
  const token = userStoreHook.token;
  // 如果不是登录页面且未登录时
  if (!token && toName !== 'Login') {
    return { name: 'Login', replace: true };
  }
  if (token) {
    // 如果已经登录获取菜单
    layoutStoreHook.init();
  }
  // 已经登录 且是登录页时
  if (token && toName === 'Login') {
    return { path: '/', replace: true };
  }
});
export { router };
