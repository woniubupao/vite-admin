import type { RouteRecordRaw } from 'vue-router';

const router: RouteRecordRaw[] = [
  // 权限组
  {
    name: 'RuleGroup',
    path: '/rule',
    component: () => import('@/views/rule-group/index.vue'),
  },
];

export default router;
