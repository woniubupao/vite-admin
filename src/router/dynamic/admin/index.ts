import type { RouteRecordRaw } from 'vue-router';

const router: RouteRecordRaw[] = [
  {
    name: 'Admin',
    path: '/admin',
    component: () => import('@/views/admin/index.vue'),
  },
];

export default router;
