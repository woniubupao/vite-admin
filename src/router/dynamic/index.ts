import goods from './goods';
import admin from './admin';

export default [
  ...goods,
  ...admin,
];
