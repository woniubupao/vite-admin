import type { RouteRecordRaw } from 'vue-router';
import Test from '@/views/test.vue';
import Goods from '@/views/goods/goods-amdin/index.vue';

const router: RouteRecordRaw[] = [
  {
    name: 'Test',
    path: '/test',
    component: Test,
  },
  {
    name: 'GoodsList',
    path: '/goods/list',
    component: Goods,
  },
];

export default router;
